# app/models/client.rb
class Client < ApplicationRecord
    has_many :appointments
    has_many :job_orders, through: :appointments
    validates :name, presence: true
  end