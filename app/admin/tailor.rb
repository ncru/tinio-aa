# app/admin/tailor.rb
ActiveAdmin.register Tailor do
    permit_params :name, :specialty
  
    index do
      selectable_column
      id_column
      column :name
      column :specialty
      actions
    end
  
    filter :name
    filter :specialty
  
    form do |f|
      f.inputs "Tailor Details" do
        f.input :name
        f.input :specialty
      end
      f.actions
    end
  end
