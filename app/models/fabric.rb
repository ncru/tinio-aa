# app/models/fabric.rb
class Fabric < ApplicationRecord
    validates :name, presence: true
    has_many :job_orders
  end