# app/models/appointment.rb
class Appointment < ApplicationRecord
    belongs_to :client
    has_many :job_orders
  end