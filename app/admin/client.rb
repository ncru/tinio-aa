# app/admin/client.rb
ActiveAdmin.register Client do
    menu label: 'Clients'
    permit_params :name, :email, :phone, :address, :city

    form do |f|
        f.inputs "Client Details" do
          f.input :name
          f.input :email
          f.input :phone
          f.input :address
          f.input :city
        end
        f.actions
      end

end
