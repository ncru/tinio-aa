# app/admin/job_order.rb
ActiveAdmin.register JobOrder do
  permit_params :appointment_id, :tailor_id, :fabric_id, :started_at, :completed_at, :status, job_order_progresses_attributes: [:id, :started_at, :completed_at, :status, :_destroy]


  index do
    selectable_column
    id_column
    column :appointment
    column :tailor
    column :fabric
    column :started_at
    column :completed_at
    column :status
    actions
  end

  filter :appointment
  filter :tailor
  filter :fabric
  filter :started_at
  filter :completed_at
  filter :status

  form do |f|
    f.inputs "Job Order Details" do
      f.input :appointment
      f.input :tailor
      f.input :fabric
      f.input :started_at, as: :datetime_picker
      f.input :completed_at, as: :datetime_picker
      f.input :status, as: :select, collection: ['pending', 'done']
    end

    f.inputs "Job Order Progresses" do
      f.has_many :job_order_progresses, allow_destroy: true, heading: 'Progress Entries', new_record: 'Add Progress Entry' do |jp|
        jp.input :started_at, as: :datetime_picker
        jp.input :completed_at, as: :datetime_picker
        jp.input :status, as: :select, collection: ['pending', 'done']
      end
    end

    f.actions
  end
end
