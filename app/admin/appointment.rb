# app/admin/appointment.rb
ActiveAdmin.register Appointment do
    permit_params :client_id, :appointment_date
  
    index do
      selectable_column
      id_column
      column :client
      column :appointment_date
      actions
    end
  
    filter :client
    filter :appointment_date
  
    form do |f|
      f.inputs "Appointment Details" do
        f.input :client
        f.input :appointment_date, as: :datepicker
      end
      f.actions
    end
  end