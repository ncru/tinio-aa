class AddAttributesToFabrics < ActiveRecord::Migration[7.0]
  def change
    add_column :fabrics, :name, :string
    add_column :fabrics, :color, :string
    add_column :fabrics, :material, :string
  end
end
