# app/models/job_order.rb
class JobOrder < ApplicationRecord
  belongs_to :appointment
  belongs_to :tailor
  belongs_to :fabric
  has_many :job_order_progresses, inverse_of: :job_order
  accepts_nested_attributes_for :job_order_progresses, allow_destroy: true
  # Rest of the model code
end
