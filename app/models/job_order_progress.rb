# app/models/job_order_progress.rb
class JobOrderProgress < ApplicationRecord
    belongs_to :job_order, inverse_of: :job_order_progresses
  end