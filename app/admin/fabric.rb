# app/admin/fabric.rb
ActiveAdmin.register Fabric do
    permit_params :name, :color, :material
  
    index do
      selectable_column
      id_column
      column :name
      column :color
      column :material
      actions
    end
  
    filter :name
    filter :color
    filter :material
  
    form do |f|
      f.inputs "Fabric Details" do
        f.input :name
        f.input :color
        f.input :material
      end
      f.actions
    end
  end