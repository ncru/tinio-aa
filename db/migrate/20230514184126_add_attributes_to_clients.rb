class AddAttributesToClients < ActiveRecord::Migration[7.0]
  def change
    add_column :clients, :name, :string
    add_column :clients, :email, :string
    add_column :clients, :phone, :string
    add_column :clients, :address, :string
    add_column :clients, :city, :string
  end
end
